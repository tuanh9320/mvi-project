from web3 import Web3
from getContractAbi import getAbi
from swapAbi import getInterface
from MVIAbi import getMVIAbi
from MVIIndexAbi import getMVIIndexAbi
from SetTokenAbi import getSetTokenAbi
import sys
sys.path.append('../Calculation')
from index import getAllParameter


from Token import getERC20Abi
from eth_abi import encode_abi
from decimal import *
import time
getcontext().prec = 100
time1 = time.time()
w3 = Web3(Web3.HTTPProvider("https://goerli.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
walletAddress = Web3.toChecksumAddress('0x552f3b71A0743d728e5c621106158134c36efF5e')
private_key = "6f018b60eb24631184b46196bfe8590038ad9e313fb74d4663c5a1b30fc9b5e8"
contractAddress = '0x770BE48c6Ecb7E2260FC0F94be6A851DAe5C36ab'
mviTokenAddress = '0xa9519A18298C5541A1738FbeBB895870e48e3451'
uniswapAddress = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D"
sushiswapAddress = "0x1b02dA8Cb0d097eB8D57A175b88c7D8b47997506"
wethTokenAddress = '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6'
basicIssuanceModuleAddress = "0x4D0A81BDf20e20Ad5556B073b12244135B40B13C"

MVIToken = w3.eth.contract(address=mviTokenAddress, abi=getMVIAbi())
uniswap = w3.eth.contract(address=uniswapAddress, abi=getInterface())
sushiswap = w3.eth.contract(address=sushiswapAddress, abi=getInterface())
WETHToken = w3.eth.contract(address=wethTokenAddress, abi=getERC20Abi())
MVIIndex = w3.eth.contract(address=basicIssuanceModuleAddress, abi=getMVIIndexAbi())
contract = w3.eth.contract(address=contractAddress, abi=getAbi())
basicIssuanceModule = w3.eth.contract(address=basicIssuanceModuleAddress, abi=getMVIIndexAbi())
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#Get all parametor calculated in the index.py file
calculationResult = getAllParameter()
print(calculationResult)
mintData = []
for i in calculationResult[4]:
    mintData.append(Decimal(i)/10**18)
def getAmountChildTokens(amountMvi):
    result = []
    for i in mintData:
        result.append(Decimal(calculationResult[0])*i)
    return result
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#Transfer WETH to contract
transaction_1 = WETHToken.functions.transfer(contractAddress, 4000000000000000000).buildTransaction({
    'chainId':5,
    'from': walletAddress,
    'gas': 700000,
    'maxFeePerGas': 57562177587,
    'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
    'nonce': w3.eth.getTransactionCount(walletAddress)
})
# signed_txn_1 = w3.eth.account.sign_transaction(transaction_1, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_1.rawTransaction)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
profitToken = wethTokenAddress
navigativeProfit = False
ethAmountToCoinBase = 100000000000
target = []
value=[]
payload=[]
timestamp = int((time.time() + 100000000000)//1)
#The function that automatical append param to target, value, payload
SetToken = w3.eth.contract(address=mviTokenAddress, abi=getSetTokenAbi())
tokenAddress = SetToken.functions.getComponents().call()#Array contain the address of child tokens
def swapToken(token, quantity, typeSwap, index):#approve WETH for DEX, swap weth to token then approve token to MVI
    token_ = w3.eth.contract(address=token, abi=getERC20Abi())
    amoutOfToken = int(calculationResult[0]*mintData[index])
    if typeSwap == 1:
        target.append(wethTokenAddress)
        target.append(uniswapAddress)
        value.append(0)
        value.append(0)
        payload.append(WETHToken.encodeABI(fn_name="approve", args=[uniswapAddress, 1000000000000000000]))#approve
        payload.append(uniswap.encodeABI(fn_name="swapTokensForExactTokens", args=[amoutOfToken, 100000000000000000000, [wethTokenAddress, token], contractAddress, timestamp]))#swap WETH to token
    else:
        target.append(wethTokenAddress)
        target.append(sushiswapAddress)
        value.append(0)
        value.append(0)
        payload.append(WETHToken.encodeABI(fn_name="approve", args=[sushiswapAddress, 1000000000000000000]))#approve
        payload.append(sushiswap.encodeABI(fn_name="swapTokensForExactTokens", args=[amoutOfToken, 1000000000000000000, [wethTokenAddress, token], contractAddress, timestamp]))#swap token to WETH
#For loop used to swap weth to child token
for i in range(len(calculationResult[1])):
    if calculationResult[1][i] == 2:
        swapToken(tokenAddress[i],calculationResult[5][i], 2, i)
    else:
        swapToken(tokenAddress[i],calculationResult[5][i], 1, i)
#For loop used to approve token to MVI
for i in range(len(calculationResult[1])):
    token = w3.eth.contract(address=tokenAddress[i], abi=getERC20Abi())
    amoutOfToken = int(calculationResult[0]*mintData[i])
    target.append(tokenAddress[i])
    value.append(0)
    payload.append(token.encodeABI(fn_name="approve", args=[basicIssuanceModuleAddress, amoutOfToken]))
target.append(basicIssuanceModuleAddress)
value.append(0)
payload.append(MVIIndex.encodeABI(fn_name="issue", args=[mviTokenAddress, calculationResult[0], contractAddress]))
if calculationResult[3] == 2:
    target.append(mviTokenAddress)
    value.append(0)
    payload.append(MVIToken.encodeABI(fn_name="approve", args=[sushiswapAddress, calculationResult[0]]))
    value.append(0)
    target.append(sushiswapAddress)
    payload.append(sushiswap.encodeABI(fn_name="swapExactTokensForTokens", args=[calculationResult[0],0,[mviTokenAddress,wethTokenAddress], contractAddress, timestamp]))
else:
    target.append(mviTokenAddress)
    value.append(0)
    payload.append(MVIToken.encodeABI(fn_name="approve", args=[uniswapAddress, calculationResult[0]]))
    value.append(0)
    target.append(sushiswapAddress)
    payload.append(uniswap.encodeABI(fn_name="swapExactTokensForTokens", args=[calculationResult[0],0,[mviTokenAddress,wethTokenAddress], contractAddress, timestamp]))

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
nonce = w3.eth.getTransactionCount(walletAddress)
gasPrice =1000000007
transaction_2 = contract.functions.trade(profitToken, 0,navigativeProfit, 0, target,value, payload).buildTransaction({
    'chainId':5,
    'from': walletAddress,
    'value':0,
    'gas': 8000000,
    'gasPrice': gasPrice,
    'nonce': nonce
})
try:
    print(WETHToken.functions.balanceOf(contractAddress).call())
    dogt = w3.eth.contract(address="0x66B72bE3d46714D729f2392c1888E459AC12d756", abi=getMVIAbi())
    print(dogt.functions.balanceOf(contractAddress).call())
    catt = w3.eth.contract(address="0x524249f31A9c5bD06fF8b651E21B77Ce9E2F4a36", abi=getMVIAbi())
    print(dogt.functions.balanceOf(contractAddress).call())
    dogt = w3.eth.contract(address="0xA56d3558eE8a5b828881372Bc5266a7707AA1793", abi=getMVIAbi())
    print(dogt.functions.balanceOf(contractAddress).call())
    # signed_txn_2 = w3.eth.account.sign_transaction(transaction_2, private_key=private_key)
    # w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)
except Exception as e:
    print(e)


#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#Code clear child token
# target_ = ["0xa9519A18298C5541A1738FbeBB895870e48e3451"]
# value_ = [0]
# childToken = w3.eth.contract(address=mviTokenAddress, abi=getMVIAbi())
# amount = childToken.functions.balanceOf(contractAddress).call()
# print(amount)
# payload_ = [childToken.encodeABI(fn_name="transfer", args=[walletAddress, 2334010367927240851942])]
# transaction_3 = contract.functions.trade(profitToken, 0,navigativeProfit, 0, target_,value_, payload_).buildTransaction({
#     'chainId':5,
#     'from': walletAddress,
#     'value':0,
#     'gas': 8000000,
#     'gasPrice': gasPrice,
#     'nonce': nonce
# })
# signed_txn_3 = w3.eth.account.sign_transaction(transaction_3, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_3.rawTransaction)