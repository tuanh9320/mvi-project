from web3 import Web3
from getContractAbi import getAbi
from swapAbi import getInterface
from MVIAbi import getMVIAbi
from MVIIndexAbi import getMVIIndexAbi
from SetTokenAbi import getSetTokenAbi
import sys
sys.path.append('../Calculation')
from index import getAllParameter
from Token import getERC20Abi
import time
from eth_abi import encode_abi

w3 = Web3(Web3.HTTPProvider("https://goerli.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))

uniswapAddress = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D"
sushiswapAddress = "0x1b02dA8Cb0d097eB8D57A175b88c7D8b47997506"
wethTokenAddress = '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6'
walletAddress = '0x552f3b71A0743d728e5c621106158134c36efF5e'
private_key = "6f018b60eb24631184b46196bfe8590038ad9e313fb74d4663c5a1b30fc9b5e8"
contractAddress = '0x770BE48c6Ecb7E2260FC0F94be6A851DAe5C36ab'

uniswap = w3.eth.contract(address=uniswapAddress, abi=getInterface())
sushiswap = w3.eth.contract(address=sushiswapAddress, abi=getInterface())
contract = w3.eth.contract(address=contractAddress, abi=getAbi())
profitToken = wethTokenAddress
navigativeProfit=False

def swapToken(tokenIn, tokenOut, amount):#approve WETH for DEX, swap weth to token then approve token to MVI
    tokenIn_ = w3.eth.contract(address=tokenIn, abi=getERC20Abi())
    timestamp = int((time.time() + 100000000000)//1)
    target = []
    value = []
    payload = []
    target.append(tokenIn)
    target.append(uniswapAddress)
    value.append(0)
    value.append(0)
    payload.append(tokenIn_.encodeABI(fn_name="approve", args=[uniswapAddress, amount]))
    payload.append(uniswap.encodeABI(fn_name="swapExactTokensForTokens", args=[amount, 0, [tokenIn, tokenOut], contractAddress, timestamp]))#swap WETH to token

    nonce = w3.eth.getTransactionCount(walletAddress)
    gasPrice =1000000007
    transaction_2 = contract.functions.trade(profitToken, 0,navigativeProfit, 0, target,value, payload).buildTransaction({
        'chainId':5,
        'from': walletAddress,
        'value':0,
        'gas': 8000000,
        'gasPrice': gasPrice,
        'nonce': nonce
    })
    signed_txn_2 = w3.eth.account.sign_transaction(transaction_2, private_key=private_key)
    w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)
swapToken("0x524249f31A9c5bD06fF8b651E21B77Ce9E2F4a36", "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6", 5000000000000000000000)

# token = w3.eth.contract(address="0x524249f31A9c5bD06fF8b651E21B77Ce9E2F4a36", abi=getERC20Abi())

# nonce = w3.eth.getTransactionCount(walletAddress)
# gasPrice =1000000007    
# transaction_3 = token.functions.transfer(contractAddress, 5000000000000000000000).buildTransaction({
#         'chainId':5,
#         'from': walletAddress,
#         'value':0,
#         'gas': 8000000,
#         'gasPrice': gasPrice,
#         'nonce': nonce
#     })
# signed_txn_3 = w3.eth.account.sign_transaction(transaction_3, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_3.rawTransaction)    