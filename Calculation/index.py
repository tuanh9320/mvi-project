from web3 import Web3
from PairAbi import getPairAbi
from abiMVI import getMVIAbi
from Erc20Abi import getErc20Abi
from sushiABI import getSushiABI
from UniswapPairAddress import getUniswapPair
from FactoryAbi import getFactoryAbi
from swapAbi import getInterface
import threading
import time
from decimal import *
import threading
time1 = time.time()
w3 = Web3(Web3.HTTPProvider("https://goerli.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
getcontext().prec = 28
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get address of child token
childToken = w3.eth.contract(address="0x4D0A81BDf20e20Ad5556B073b12244135B40B13C", abi=getMVIAbi())
data = childToken.functions.getRequiredComponentUnitsForIssue(Web3.toChecksumAddress('0xa9519A18298C5541A1738FbeBB895870e48e3451'), 1000000000000000000).call()
tokenAddress = data[0]
quantityOfChildToken = len(tokenAddress)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get decimal of all token
decimals = []
for i in tokenAddress:
	temp = w3.eth.contract(address=Web3.toChecksumAddress(i), abi=getErc20Abi())
	decimals.append(temp.functions.decimals().call())
decimals.append(18)
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get the number of child token that required to mint a BMVI token
mintData = []
count = 0
for i in data[1]:
	mintData.append(Decimal(i)/Decimal(10**decimals[count]))
	count += 1
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get all pair address
sushiPairAddress = []
uniPairAddress = []
sushiFactory = w3.eth.contract(address="0xc35DADB65012eC5796536bD9864eD8773aBc74C4", abi=getFactoryAbi())
uniFactory = w3.eth.contract(address="0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f", abi=getFactoryAbi())
for token in tokenAddress:
	uniPairAddress.append(uniFactory.functions.getPair(token, "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6").call())
	sushiPairAddress.append(sushiFactory.functions.getPair(token, "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6").call())
uniPairAddress.append(uniFactory.functions.getPair("0xa9519A18298C5541A1738FbeBB895870e48e3451", "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6").call())#MVI-WETH
sushiPairAddress.append(sushiFactory.functions.getPair("0xa9519A18298C5541A1738FbeBB895870e48e3451", "0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6").call())#MVI-WETH
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#get reserves
wethReservesUNI = 0
mviReservesUNI = 0
wethReservesSUSHI = 0
mviReservesSUSHI = 0
pairNotExistInUni = []
pairNotExistInSushi = []
reservesUNI = []
count1 = 0
allParameter = []
for i in uniPairAddress:
	if i == '0x0000000000000000000000000000000000000000':
		reservesUNI.append(0)
		reservesUNI.append(0)
		pairNotExistInUni.append(count1)
	else:
		pair = w3.eth.contract(address=i, abi=getPairAbi())
		reserves = pair.functions.getReserves().call()
		token0 = pair.functions.token0().call()
		token1 = pair.functions.token1().call()
		if token0 == '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6':
			if token1 == "0xa9519A18298C5541A1738FbeBB895870e48e3451":
				wethReservesUNI = reserves[0]
				mviReservesUNI = reserves[1]
			reservesUNI.append(Decimal(reserves[0])/Decimal(1000000000000000000))
			reservesUNI.append(Decimal(reserves[1])/Decimal(10**decimals[count1]))
		else:
			if token0 == "0xa9519A18298C5541A1738FbeBB895870e48e3451":
				wethReservesUNI = reserves[1]
				mviReservesUNI = reserves[0]
			reservesUNI.append(Decimal(reserves[1])/Decimal(1000000000000000000))
			reservesUNI.append(Decimal(reserves[0])/Decimal(10**decimals[count1]))
	count1 += 1
count2 = 0
reservesSUSHI = []
for i in sushiPairAddress:
	if i == '0x0000000000000000000000000000000000000000':
		reservesSUSHI.append(0)
		reservesSUSHI.append(0)
		pairNotExistInSushi.append(count2)
	else:
		pair = w3.eth.contract(address=i, abi=getPairAbi())
		reserves = pair.functions.getReserves().call()
		token0 = pair.functions.token0().call()
		token1 = pair.functions.token1().call()
		if token0 == '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6':
			if token1 == "0xa9519A18298C5541A1738FbeBB895870e48e3451":
				wethReservesSUSHI = reserves[0]
				mviReservesSUSHI = reserves[1]
			reservesSUSHI.append(Decimal(reserves[0])/Decimal(1000000000000000000))
			reservesSUSHI.append(Decimal(reserves[1])/Decimal(10**decimals[count2]))
		else:
			if token0 == "0xa9519A18298C5541A1738FbeBB895870e48e3451":
				wethReservesSUSHI = reserves[1]
				mviReservesSUSHI = reserves[0]
			reservesSUSHI.append(Decimal(reserves[1])/Decimal(1000000000000000000))
			reservesSUSHI.append(Decimal(reserves[0])/Decimal(10**decimals[count2]))
	count2 += 1
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#Func that determine the maximum amout of MVI token that we can reach
maximumToken = []
def getMaxBmvi():
	maximumBmvi = 0
	for i in range(quantityOfChildToken):
		temp = 2*i + 1
		if reservesUNI[temp] > reservesSUSHI[temp]:
			maximumToken.append([reservesUNI[temp], 1])
		else:
			maximumToken.append([reservesSUSHI[temp], 2])
	for i in range(quantityOfChildToken):
		temp = maximumToken[i][0]/mintData[i]
		if i == 0:
			maximumBmvi = temp
		elif temp < maximumBmvi:
			maximumBmvi = temp
	return maximumBmvi
#This function calculate profit when mint y MVI token
def calculateProfit(y):
	return_value = []
	y = Decimal(y)
	typeSwap = []#array contain the type used to swap for each pair
	amountWethOut_Uni = (997*y*reservesUNI[2*quantityOfChildToken])/(1000*reservesUNI[2*quantityOfChildToken + 1] + 997*y)
	amountWethOut_Sushi = (997*y*reservesSUSHI[2*quantityOfChildToken])/(1000*reservesSUSHI[2*quantityOfChildToken + 1] + 997*y)
	if amountWethOut_Uni > amountWethOut_Sushi:
		return_value.append(1)#Use UNI to swap from BMVI to WETH
		profit = amountWethOut_Uni
		for i in range(quantityOfChildToken):
			wethIn_Uni = (1000*reservesUNI[2*i]*mintData[i]*y)/(997*reservesUNI[2*i + 1]-997*mintData[i]*y)
			wethIn_Sushi = (1000*reservesSUSHI[2*i]*mintData[i]*y)/(997*reservesSUSHI[2*i + 1]-997*mintData[i]*y)
			#we have a step calculate the maximum amount of BMVI that we can reach, so the case wethIn_Uni and wethIn_Sushi are less then 0 will not happen
			if wethIn_Uni < 0:#Case that pair in UNI don't have enought child token
				profit = Decimal(profit - wethIn_Sushi)
				typeSwap.append(2)
			elif wethIn_Uni < wethIn_Sushi or wethIn_Sushi < 0:
				profit = Decimal(profit - wethIn_Uni)
				typeSwap.append(1)
			else:
				profit = Decimal(profit - wethIn_Sushi)
				typeSwap.append(2)
	else:
		return_value.append(2)#Use SUSHI to swap from BMVI to WETH
		profit = amountWethOut_Sushi
		for i in range(quantityOfChildToken):
			wethIn_Uni = (1000*reservesUNI[2*i]*mintData[i]*y)/(997*reservesUNI[2*i + 1]-997*mintData[i]*y)
			wethIn_Sushi = (1000*reservesSUSHI[2*i]*mintData[i]*y)/(997*reservesSUSHI[2*i + 1]-997*mintData[i]*y)
			#we have a step calculate the maximum amount of BMVI that we can reach, so the case wethIn_Uni and wethIn_Sushi are less then 0 will not happen
			if wethIn_Uni < 0:#Case that pair in UNI don't have enought child token
				profit = Decimal(profit - wethIn_Sushi)
				typeSwap.append(2)
			elif wethIn_Uni < wethIn_Sushi or wethIn_Sushi < 0:
				profit = Decimal(profit - wethIn_Uni)
				typeSwap.append(1)
			else:
				profit = Decimal(profit - wethIn_Sushi)
				typeSwap.append(2)
	return_value.append(int(profit*Decimal(10**18)))
	return_value.append(typeSwap)
	#now the return value contain typw swap from BMVI to WETH, profit, array of type DEX used to swap
	return return_value
#Use ternary search to find the amount of BMVI token to reach maximum profit
def determineAmoutBMVI(left, right):
	while right-left > 0.00000000001:
		x1 = left + (right-left)/3
		x2 = right - (right-left)/3
		if calculateProfit(x1)[1] > calculateProfit(x2)[1]:
			right=x2
		else:
			left=x1
	return right
def getAmoutWethToBuyTokens(y, typeSwap):
	result = []
	for i in range(len(typeSwap)):
		if typeSwap[i] == 1:
			result.append(round(((1000*reservesUNI[2*i]*mintData[i]*y)/(997*reservesUNI[2*i + 1]-997*mintData[i]*y))*10**18))
		else:
			result.append(round(((1000*reservesSUSHI[2*i]*mintData[i]*y)/(997*reservesSUSHI[2*i + 1]-997*mintData[i]*y))*10*18))
	return result
def getAllParameter():
	maximumBmvi = getMaxBmvi()
	amoutBMVI = determineAmoutBMVI(0,maximumBmvi)
	amoutBMVI = Decimal(amoutBMVI*10**18)
	amoutBMVI = int(amoutBMVI)
	temp = Decimal(Decimal(amoutBMVI)/10**18)
	result = calculateProfit(temp)
	amoutWethUsedToBuyTokens = getAmoutWethToBuyTokens(temp, result[2])
	return(amoutBMVI, result[2], result[1], result[0], data[1], amoutWethUsedToBuyTokens)
print(getAllParameter())
print(reservesUNI)
print(reservesSUSHI)
print(tokenAddress)